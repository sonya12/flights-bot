import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from "typeorm";
import { UsersRequests } from "./usersRequests.entity";


@Entity()
export class Requests extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ unique: true })
    name: string

    @Column({ default: 100 })
    defaultLimit: number

    @OneToMany(() => UsersRequests, reg => reg.request)
    registrRequests: UsersRequests[]
}
