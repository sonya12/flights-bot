import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";


@Entity()
export class UserPolitics extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ default: -1, type: 'int8' })
    chat_id: number

    @Column()
    politicName: string

    @Column({ type: 'json' })
    flightTypes: string[]

    @Column({ type: 'json' })
    flightRegs: string[]

    @Column({ type: 'json' })
    flightCalls: string[]
}
