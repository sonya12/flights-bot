import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";


@Entity({
    schema: "public",
    synchronize: true,
})
export class Users extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column({ default: '' })
    username: string

    @Column({ default: '' })
    first_name: string

    @Column({ default: '' })
    last_name: string

    @Column({ default: -1, type: 'int8' })
    chat_id: number
}
