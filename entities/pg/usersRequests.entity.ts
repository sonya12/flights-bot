import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, JoinColumn, RelationId, ManyToOne } from "typeorm";
import { Requests } from "./requests.entity";


@Entity()
export class UsersRequests extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    user_id: number

    @RelationId((reg: UsersRequests) => reg.request)
    @Column()
    request_id: number

    @Column({ default: 0 })
    count: number

    @Column()
    limit: number

    @Column({ default: new Date() })
    lastUpdate: Date

    @ManyToOne(() => Requests, req => req.id)
    @JoinColumn({ name: "request_id" })
    request: Requests
}
