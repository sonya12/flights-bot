import { Entity, Column, BaseEntity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Airbases extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({ type: "float4" })
    lat: number

    @Column({ type: "float4" })
    lon: number

    @Column()
    country: string

    @Column()
    type: string

    @Column({ length: 24, unique: true })
    icao: string

    @Column()
    elevation: number

    @Column()
    continent: string
}
