import { Entity, PrimaryColumn, Column, BaseEntity, Generated } from "typeorm";


@Entity()
export class FlightsEntity extends BaseEntity {

    @PrimaryColumn()
    hexcode: string

    @Column()
    @Generated("uuid")
    flight_id: string

    @Column()
    reg: string

    @Column({ default: null })
    callsign: string

    @Column({ default: null })
    type: string

    @Column({ default: null })
    country: string

    @Column({ type: "float4" })
    lat: number

    @Column({ type: "float4" })
    lon: number

    @Column({ type: 'int8' })
    last_update: number

    @Column({ type: "float4" })
    alt: number

    @Column({ type: "float4" })
    course: number

    @Column({ type: "float4" })
    speed: number

    @Column({ default: 'unknown' })
    image: string

    @Column({ type: "float4", default: 1 })
    image_scale: number
}
