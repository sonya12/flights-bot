import { FlightsEntity } from '../pg/flights.entity';
import { clickhouse } from '../../src/clickhouse';
import { AbstractRepository } from './AbstractRepository';


export class FlightsRepository extends AbstractRepository {


    async createTable(): Promise<void> {
        await clickhouse.query(
            `CREATE TABLE IF NOT EXISTS flights
            (
                id UUID,
                type String,
                callsign String,
                reg String,
                point Point,
                alt Int16,
                course UInt8,
                speed UInt16,
                timestamp DateTime
            )
            ENGINE = MergeTree()
            ORDER BY (type, id, reg)
            PARTITION BY toYYYYMM(timestamp);`)
    }

    async dropTabel(): Promise<void> {
        await clickhouse.query('DROP TABLE flights;');
    }

    static async saveFlights(flights: FlightsEntity[]) {
        const inserts: string[] = []
        for (const flight of flights) {
            const values: any = []
            values.push(`'${flight.flight_id}'`)
            values.push(`'${flight.type}'`)
            values.push(`'${flight.callsign}'`)
            values.push(`'${flight.reg}'`)
            values.push(`(${flight.lat},${flight.lon})`)
            values.push(flight.alt)
            values.push(flight.course)
            values.push(flight.speed)
            values.push(Math.round(flight.last_update / 1000))
            inserts.push(`(${values.join(',')})`)
        }
        const insertQuery = `INSERT INTO flights VALUES ${inserts.join(',')};`
        await clickhouse.query(insertQuery)
    }

}