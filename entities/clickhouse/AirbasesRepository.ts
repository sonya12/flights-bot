import { Airbases } from '../pg/airbases.entity';
import { clickhouse } from '../../src/clickhouse';
import { AbstractRepository } from './AbstractRepository';

const DB_HOST = process.env['DB_HOST'] ?? 'gateway.docker.internal:5432'
const DB_NAME = process.env['DB_NAME'] ?? 'flights-bot'
const DB_USERNAME = process.env['DB_USERNAME'] ?? 'postgres'
const DB_PASSWORD = process.env['DB_PASSWORD'] ?? 'admin'

export class AirbasesRepository extends AbstractRepository {


    async createTable(): Promise<void> {
        const tableName = Airbases.getRepository().metadata.tableName
        await clickhouse.query(
            `CREATE TABLE IF NOT EXISTS airbases
            (
                name String,
                lat Float32,
                lon Float32,
                icao String,
                country String,
                continent String
            )
            ENGINE = PostgreSQL('${DB_HOST}', '${DB_NAME}', '${tableName}', '${DB_USERNAME}', '${DB_PASSWORD}');`)
    }

    async dropTabel(): Promise<void> {
        await clickhouse.query('DROP TABLE airbases;');
    }

}