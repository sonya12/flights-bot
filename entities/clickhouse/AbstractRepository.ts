import { clickhouse } from "../../src/clickhouse";



export abstract class AbstractRepository {
    abstract createTable(): Promise<void>
    abstract dropTabel(): Promise<void>
    static async query(sql: string): Promise<any> { return await clickhouse.query(sql) }
}