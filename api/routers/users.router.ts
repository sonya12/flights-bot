import { Router } from "express"
import HttpErrorHandler from "../httpErrorHandler"
import { keycloak } from "../middleware/auth"
import { UsersService } from "../controllesr/users.controller"

const router = Router()

router.get('/', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        res.json(await UsersService.get())
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.get('/requests', keycloak.protect('realm:admin'), async (_req, res) => {
    try {
        res.json(await UsersService.usersRequsts())
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.get('/:user_id/requests', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const user_id = Number(req.params.user_id)
        res.json(await UsersService.userRequsts(user_id))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.patch('/:user_id/requests/:req_id', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const user_id = Number(req.params.user_id)
        const req_id = Number(req.params.req_id)
        const { limit } = req.body
        res.json(await UsersService.updateLimit(user_id, req_id, Number(limit)))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

export default router