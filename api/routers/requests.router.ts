import { Router } from "express"
import HttpErrorHandler from "../httpErrorHandler";
import { keycloak } from "../middleware/auth";
import { RequestsService } from "../controllesr/requests.controller";

const router = Router()

router.get('/', keycloak.protect('realm:admin'), async (_req, res) => {
    try {
        res.json(await RequestsService.get())
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const { name, defaultLimit } = req.body
        res.json(await RequestsService.create(name, defaultLimit))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.patch('/:id', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const id = Number(req.params.id)
        const { defaultLimit } = req.body
        res.json(await RequestsService.update(id, defaultLimit))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.delete('/:id', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const id = Number(req.params.id)
        res.json(await RequestsService.delete(id))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})


export default router