import { Router } from "express"
import HttpErrorHandler from "../httpErrorHandler";
import { keycloak } from "../middleware/auth";
import { AirbaseController } from "../controllesr/airbases.controller";

const router = Router()

router.get('/', keycloak.protect('realm:user'), async (_req, res) => {
    try {
        res.json(await AirbaseController.get())
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.post('/', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        res.json(await AirbaseController.create(req.body))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.patch('/:id', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const id = Number(req.params.id)
        res.json(await AirbaseController.update(id, req.body))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.delete('/:id', keycloak.protect('realm:admin'), async (req, res) => {
    try {
        const id = Number(req.params.id)
        res.json(await AirbaseController.delete(id))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

export default router