import { Router } from "express"
import HttpErrorHandler from "../httpErrorHandler";
import { keycloak } from "../middleware/auth";
import { FlightsController } from "../controllesr/flights.controller";

const router = Router()

router.get('/', keycloak.protect('realm:user'), async (_req, res) => {
    try {
        res.json(await FlightsController.get())
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

router.get('/:id/router', keycloak.protect('realm:user'), async (req, res) => {
    try {
        const { id } = req.params
        res.json(await FlightsController.routerById(id))
    } catch (error: any) {
        HttpErrorHandler.handle(error, res)
    }
})

export default router