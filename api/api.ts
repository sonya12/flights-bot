import express from 'express'
import morgan from 'morgan';
import * as prom from './middleware/prometheus'
import { authMiddleware } from './middleware/auth';
import flightsRouter from "./routers/flights.router"
import airbasesRouter from "./routers/airbases.router"
import usersRouter from './routers/users.router'
import requestsRouter from './routers/requests.router';

export const internal = express()

internal.set('port', 3050);
internal.use(express.json());
internal.get('/metrics', prom.metricsRoute)
internal.get('/healthchecks', (_req, res) => { res.end('ok') })

const app = express()

app.set('port', 3000);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(prom.middleware)

if (process.env['NODE_ENV'] !== 'dev') {
    app.use(morgan(':date[iso] :remote-addr :method :url :status :response-time ms'));
}

app.use(authMiddleware())
app.use('/flights', flightsRouter)
app.use('/airbases', airbasesRouter)
app.use('/users', usersRouter)
app.use('/requests', requestsRouter)

export { app }
