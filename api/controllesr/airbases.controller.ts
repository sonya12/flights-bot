import { Airbases } from "../../entities/pg/airbases.entity";
import { NotFoundError } from "../httpErrorHandler";


export class AirbaseController {

    static async get() {
        return await Airbases.find()
    }

    static async create(data: any) {
        const base = new Airbases()
        base.icao = data.icao
        base.name = data.name
        base.type = data.type
        base.elevation = data.elevation
        base.continent = data.continent
        base.country = data.country
        base.lat = data.lat
        base.lon = data.lon
        await base.save()
        return base
    }

    static async update(id: number, data: any) {
        const base = await Airbases.findOne({ where: { id } })
        if (!base)
            throw new NotFoundError('airbase not found')

        base.icao = data.icao ?? base.icao
        base.name = data.name ?? base.name
        base.type = data.type ?? base.type
        base.elevation = data.elevation ?? base.elevation
        base.continent = data.continent ?? base.continent
        base.country = data.country ?? base.country
        base.lat = data.lat ?? base.lat
        base.lon = data.lon ?? base.lon
        await base.save()
        return base
    }

    static async delete(id: number) {
        const base = await Airbases.findOne({ where: { id } })
        if (!base)
            throw new NotFoundError('airbase not found')

        await base.remove()
        return true
    }
}