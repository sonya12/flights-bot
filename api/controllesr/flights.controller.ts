import { MoreThan } from "typeorm";
import cache from "../../src/cache"
import { FlightsEntity } from "../../entities/pg/flights.entity";
import { fetchFlightsService } from "../../services/fetchFlightsService";


export class FlightsController {

    static async get() {
        return await FlightsEntity.find({ where: { last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)) } })
    }

    static async routerById(id: string) {
        const points = await cache.getList(id, 0, -1)
        return points.map(point => JSON.parse(point))
    }
}