import { UsersRequests } from "../../entities/pg/usersRequests.entity";
import { Requests } from "../../entities/pg/requests.entity";
import { Users } from "../../entities/pg/users.entity";
import { NotFoundError } from "../httpErrorHandler";


export class UsersService {

    static async get() {
        return await Users.find()
    }

    static async usersRequsts() {
        return await UsersRequests.find()
    }

    static async userRequsts(user_id: number) {
        return await UsersRequests.find({ where: { user_id }, relations: ['request'] })
        // return await Requests.find({ where: { registrRequests: { user_id } }, relations: ['registrRequests'] })
    }

    static async updateLimit(user_id: number, req_id: number, limit: number) {
        const userReq = await UsersRequests.findOne({ where: { user_id, request_id: req_id } })
        if (!userReq) {
            const req = await Requests.findOne({ where: { id: req_id } })
            if (!req)
                throw new NotFoundError("request not found")

            const newReg = new UsersRequests()
            newReg.limit = limit
            newReg.user_id = user_id
            newReg.request_id = req_id
            await newReg.save()
            return newReg
        }
        userReq.limit = limit
        await userReq.save()
        return userReq
    }
}