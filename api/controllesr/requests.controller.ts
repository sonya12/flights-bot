import { Requests } from "../../entities/pg/requests.entity";
import { NotFoundError } from "../httpErrorHandler";


export class RequestsService {

    static async get() {
        return await Requests.find()
    }

    static async create(name: string, defaultLimit: number) {
        const req = new Requests()
        req.name = name
        req.defaultLimit = defaultLimit
        await req.save()
        return req
    }


    static async update(id: number, defaultLimit: number) {
        const req = await Requests.findOne({ where: { id } })
        if (!req)
            throw new NotFoundError('request not found')

        req.defaultLimit = defaultLimit ?? req.defaultLimit
        await req.save()
        return req
    }

    static async delete(id: number) {
        const req = await Requests.findOne({ where: { id } })
        if (!req)
            throw new NotFoundError('request not found')

        await req.remove()
        return true
    }
}