import { bot } from './bot/bot'
import cache from './src/cache'
import { AppDataSource } from './src/db'
import queue from './src/queue'
import { app, internal } from './api/api'
import commandChannel from './queue/events'
import { fetchFlightsService } from './services/fetchFlightsService'
import { clickhouse } from './src/clickhouse'
import { FlightsRepository } from './entities/clickhouse/FlightsRepository'
import { HistoricalFlightService } from './services/historicalFlightService'
import { AirbasesRepository } from './entities/clickhouse/AirbasesRepository'


const APIGATEWAY_URL = process.env['APIGATEWAY_URL'] ?? 'http://localhost'
const IS_DEV = process.env['NODE_ENV'] == 'dev'

process.once('SIGINT', () => {
    console.log('SIGINT')
    bot.stop('SIGINT')
    process.exit(1)
})
process.once('SIGTERM', () => {
    console.log('SIGTERM')
    bot.stop('SIGTERM')
    process.exit(2)
})


async function run() {
    await AppDataSource.initialize()
    console.log('[OK] DB is connected')

    await cache.connect()
    console.log('[OK] Cache is connected!')

    await queue.connect()
    await commandChannel.listen()
    console.log('[OK] Queue is connected!')

    await clickhouse.initialize();
    console.log('[OK] Column DB is connected!')

    setInterval(async () => {
        await HistoricalFlightService.save()
    }, 60000)

    if (!IS_DEV) {
        const botWebhookUrl = `/webhook/${bot.secretPathComponent()}`
        bot.telegram.setWebhook(APIGATEWAY_URL + botWebhookUrl)
        app.use(bot.webhookCallback(botWebhookUrl))
    } else {
        bot.launch()
    }

    await fetchFlightsService.initialize()
    fetchFlightsService.startUpdate()

    app.listen(app.get('port'), () => {
        console.log('[OK] Server is running!')
    })

    internal.listen(internal.get('port'), () => {
        console.log('[OK] Internal  server is running!')
    })
}

run()
    .then(() => {

    })
    .catch((e) => {
        console.log(e)
        process.exit(1)
    })