import { NowFlight } from "./flights";


//by type [] E6 B742 B52 B1 R135 K35R C17 C5M C130 P8 BE20 Q4
//by callsign
export interface FilterOptions {
    type?: string
    callsing?: string
    country?: string
    reg?: string
}



export function filterFlights(flights: NowFlight[], filters: FilterOptions[]) {
    const result: NowFlight[] = []

    for (const flight of flights) {
        for (const filter of filters) {
            if ((filter.type ? flight.type == filter.type : true)
                && (filter.callsing ? flight.callsign.startsWith(filter.callsing) : true)
                && (filter.country ? flight.country == filter.country : true)
                && (filter.reg ? flight.reg == filter.reg : true)) {
                result.push(flight)
                break
            }
        }
    }
    return result
}