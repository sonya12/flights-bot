import fs from "fs"

type AircraftIcons = {
    [key: string]: [string, number]
}
let TypeDesignatorIcons: AircraftIcons = {}
let TypeDescriptionIcons: AircraftIcons = {}
let CategoryIcons: AircraftIcons = {}
let types: any = {}

function init() {
    types = JSON.parse(fs.readFileSync(__dirname + '/../files/aircraft_types.json').toString())

    CategoryIcons = JSON.parse(fs.readFileSync(__dirname + '/../files/category_icons.json').toString())
    TypeDesignatorIcons = JSON.parse(fs.readFileSync(__dirname + '/../files/designator_icons.json').toString())
    TypeDescriptionIcons = JSON.parse(fs.readFileSync(__dirname + '/../files/description_icons.json').toString())
}

init()


export function flightImages(category: string, typeDesignator: string): [string, number] {

    if (typeDesignator in TypeDesignatorIcons) {
        return TypeDesignatorIcons[typeDesignator]
    }

    //read typeDescription and wtc from file
    if (types[typeDesignator]) {

        const [, typeDescription, wtc] = types[typeDesignator]

        //check typeDescription and wtc
        if (typeDescription && typeDescription.length === 3) {
            if (typeDescription === "L1P" && category === "B4") {
                return ["cessna", 0.92];
            }
            if (wtc && wtc.length === 1) {
                let typeDescriptionWithWtc = typeDescription + "-" + wtc;
                if (typeDescriptionWithWtc === "L2J-M" && category === "A2") {
                    return ["jet_swept", 1];
                }
                if (typeDescriptionWithWtc in TypeDescriptionIcons) {
                    return TypeDescriptionIcons[typeDescriptionWithWtc];
                }
            }
            if (typeDescription in TypeDescriptionIcons) {
                return TypeDescriptionIcons[typeDescription];
            }
            let basicType = typeDescription.charAt(0);
            if (basicType in TypeDescriptionIcons) {
                return [TypeDescriptionIcons[basicType][0], 1];
            }
        }
    }

    if (category && category in CategoryIcons) {
        return CategoryIcons[category]
    }

    return ['unknown', 1]
}