import amqp from "amqplib"


const RABBITMQ_HOST = process.env['RABBITMQ_HOST'] ?? 'localhost'
const RABBITMQ_PORT = process.env['RABBITMQ_PORT'] ?? '5672'
const RABBITMQ_USERNAME = process.env['RABBITMQ_USERNAME'] ?? 'queue'
const RABBITMQ_PASSWORD = process.env['RABBITMQ_PASSWORD'] ?? 'queue'

type Handler = {
    event: string
    fn: (msg: any, ack: () => void) => void | Promise<void>
}

type Channels = {
    [key: string]: Channel,
}

let connection: amqp.Connection;

class Queue {

    private options: amqp.Options.Connect
    private channels: Channels = {}

    constructor(hostname: string, port: number | string, username?: string, password?: string) {
        this.options = {
            hostname,
            port: Number(port),
            username,
            password
        }
    }

    async connect() {
        connection = await amqp.connect(this.options)
    }

    createChannel(name: string, queueOptions: amqp.Options.AssertQueue, consumeOptions: amqp.Options.Consume): Channel {
        if (this.channels[name]) {
            return this.channels[name]
        } else {
            const channel = new Channel(name, queueOptions, consumeOptions)
            this.channels[name] = channel
            return channel
        }
    }
}

export class Channel {

    private channel: amqp.Channel
    private name: string
    private queueOptions?: amqp.Options.AssertQueue
    private consumeOptions?: amqp.Options.AssertQueue
    private handlers: Handler[] = []
    private _prefetch: number = -1

    constructor(name: string, queueOptions: amqp.Options.AssertQueue, consumeOptions: amqp.Options.Consume) {
        this.name = name
        this.queueOptions = queueOptions
        this.consumeOptions = consumeOptions
    }

    prefetch(count: number) {
        this._prefetch = count
    }

    async listen() {
        this.channel = await connection.createChannel()
        await this.channel.assertQueue(this.name, this.queueOptions)
        if (this._prefetch != -1) {
            await this.channel.prefetch(this._prefetch)
        }

        this.channel.consume(this.name, async (msg) => {
            if (msg !== null) {
                const json = JSON.parse(msg.content.toString())
                const handler = this.handlers.find(h => {
                    return h.event == json.event
                })
                if (typeof json.data == 'string') {
                    json.data = JSON.parse(json.data)
                }
                console.log(new Date(), `event received: ${json.event}`)
                if (handler) {
                    await handler.fn(json, () => this.channel.ack(msg))
                } else {
                    console.error(`Handler for ${json.event} is undefined. Skip the event`)
                    this.channel.ack(msg)
                }
            }
        }, this.consumeOptions)
    }

    event(event: string, fn: (msg: any, ack: () => void) => Promise<void> | void) {
        this.handlers.push({
            event,
            fn
        })
    }

}

const queue = new Queue(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USERNAME, RABBITMQ_PASSWORD)

export default queue