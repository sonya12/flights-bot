import { ClickHouse } from 'clickhouse';
import { FlightsRepository } from '../entities/clickhouse/FlightsRepository';
import { AirbasesRepository } from '../entities/clickhouse/AirbasesRepository';
import { AbstractRepository } from '../entities/clickhouse/AbstractRepository';

const CLICKHOUSE_HOST = process.env['CLICKHOUSE_HOST'] ?? 'localhost'
const CLICKHOUSE_DBNAME = process.env['CLICKHOUSE_DBNAME'] ?? 'flights'
const CLICKHOUSE_USERNAME = process.env['CLICKHOUSE_USERNAME'] ?? 'default'
const CLICKHOUSE_PASSWORD = process.env['CLICKHOUSE_PASSWORD'] ?? 'default'

class Clickhouse {

    private client: ClickHouse
    private repositories: (typeof AirbasesRepository)[] = [
        FlightsRepository,
        AirbasesRepository
    ]

    constructor() {
        this.client = new ClickHouse({
            url: `http://${CLICKHOUSE_HOST}`,
            port: 8123,
            debug: false,
            basicAuth: CLICKHOUSE_PASSWORD ? { username: CLICKHOUSE_USERNAME, password: CLICKHOUSE_PASSWORD } : null,
            isUseGzip: false,
            trimQuery: true,
            usePost: false,
            format: "json",
            raw: false,
            config: {
                session_id: 'session_id if neeed',
                session_timeout: 60,
                output_format_json_quote_64bit_integers: 0,
                enable_http_compression: 1,
                database: 'default',
            },
        });
    }

    async initialize() {
        await this.client.query(`CREATE DATABASE IF NOT EXISTS ${CLICKHOUSE_DBNAME};`).toPromise();
        await this.client.query(`USE ${CLICKHOUSE_DBNAME};`).toPromise();
        //set database for clickhouse client
        (<any>this.client).opts.database = CLICKHOUSE_DBNAME;
        (<any>this.client).opts.config.database = CLICKHOUSE_DBNAME
        await this.client.query('SELECT 1;', { database: CLICKHOUSE_DBNAME }).toPromise();

        for (const repo of this.repositories) {
            const entity = new repo()
            await entity.createTable()            
        }
    }

    async query(query: string) {
        return await this.client.query(query).toPromise()
    }
}

const clickhouse = new Clickhouse()

export { clickhouse }

