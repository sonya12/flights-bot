import "reflect-metadata";
import { DataSource } from "typeorm";
import { FlightsEntity } from "../entities/pg/flights.entity";
import { Requests } from "../entities/pg/requests.entity";
import { UserPolitics } from "../entities/pg/userPolitics.entity";
import { Users } from "../entities/pg/users.entity";
import { UsersRequests } from "../entities/pg/usersRequests.entity";
import { Airbases } from "../entities/pg/airbases.entity";

const DB_HOST = process.env['DB_HOST'] ?? 'localhost'
const DB_NAME = process.env['DB_NAME'] ?? 'flights-bot'
const DB_USERNAME = process.env['DB_USERNAME'] ?? 'postgres'
const DB_PASSWORD = process.env['DB_PASSWORD'] ?? 'admin'


export const AppDataSource = new DataSource({
    type: "postgres",
    host: DB_HOST,
    port: 5432,
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    entities: [
        "./entities/pg/*[.ts]", //Not work
        Requests, UserPolitics, Users, UsersRequests, FlightsEntity, Airbases
    ],
    migrations: [
        "./entities/migrations/*[.ts]"
    ],
    migrationsRun: true,
    synchronize: true
})
