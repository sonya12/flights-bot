
export interface NowFlight {
    reg: string
    callsign: string
    hexcode: string
    type: string
    country: string    
    image: [string, number]

    lat: number
    lon: number
    timestamp: number
    alt: number
    course: number
    speed: number
}