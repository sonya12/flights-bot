import fs from "fs"

type AircraftFlag = {
    start: string
    end: string
    country: string
    flag_image: string
}

const unassigned_range = {
    country: "Unassigned",
    flag_image: null
} as const;

let flags: AircraftFlag[] = []

function init() {
    flags = JSON.parse(fs.readFileSync(__dirname + '/../files/flags.json').toString())
}

init()

export function flagByHexcode(hexcode: string) {
    let hexa = +("0x" + hexcode);

    for (let i = 0; i < flags.length; ++i) {
        if (hexa >= Number(flags[i].start) && hexa <= Number(flags[i].end)) {
            return flags[i].country
            // return {
            //     name: flags[i].country,
            //     image: flags[i].flag_image
            // };
        }
    }
    return unassigned_range.country;
}