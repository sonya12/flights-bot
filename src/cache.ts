import { RedisClientType } from '@redis/client';
import { createClient } from 'redis'

const CACHE_HOST: string = process.env['CACHE_HOST'] || 'localhost';
const CACHE_PORT: string = process.env['CACHE_PORT'] || '6379';
const IS_DEV = process.env['NODE_ENV'] == 'dev'


class Redis {

    private client: RedisClientType
    prefix: string = ''

    constructor(host: string, port: string, prefix: string = '') {
        this.client = createClient({
            url: `redis://${host}:${port}`
        })
        this.prefix = prefix
    }

    async connect() {
        this.client.on("error", function (error) {
            console.log('[ERROR] Redis isn\'t connected')
            console.error(error);
        });
        await this.client.connect()
    }

    async get(key: string) {
        return await this.client.get(this.prefix + key)
    }

    async set(key: string, data: string, seconds: number = -1) {
        if (seconds == -1)
            await this.client.set(this.prefix + key, data)
        else
            await this.client.set(this.prefix + key, data, { EX: seconds, })
    }

    async del(key: string) {
        await this.client.del(this.prefix + key)
    }

    async ttlKey(key: string) {
        return this.client.ttl(this.prefix + key)
    }

    async pushToList(key: string, value: string, seconds: number) {
        await this.client.rPush(this.prefix + key, value)
        if (seconds > 0)
            await this.client.expire(this.prefix + key, seconds, 'LT')
    }

    async getList(key: string, start: number = 0, end: number = -1) {
        return this.client.lRange(this.prefix + key, start, end)
    }

    async removeFromList(key: string, item: string) {
        this.client.lRem(this.prefix + key, 0, item)
    }

}

const cache = new Redis(CACHE_HOST, CACHE_PORT, 'flights-bot/')

export default cache