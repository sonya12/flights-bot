import { NowFlight } from "./flights";


export class MergeFlights {
    static merge(nowFlights: NowFlight[]) {
        const result = new Map();

        nowFlights.forEach((item) => {
            const unique = item.reg;
            if (result.has(unique)) {
                result.set(unique, { ...item, ...result.get(unique) })
            } else {
                result.set(unique, item);
            }
        });

        return Array.from(result.values())
    }
}