FROM node:16.13.2-alpine
WORKDIR /opt/app/
COPY . .
RUN npm ci
RUN npm run build

FROM node:16.13.2-alpine
EXPOSE 3000
WORKDIR /opt/app/
COPY --from=0 /opt/app/package*.json ./
RUN npm ci --prod
COPY --from=0 /opt/app/.dist/ ./
COPY ./files/ ./files/
CMD npm run start