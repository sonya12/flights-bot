import { bot } from "../bot/bot";
import { Users } from "../entities/pg/users.entity";
import { UsersRequests } from "../entities/pg/usersRequests.entity";



export class CommandHandler {

    static async sendTelegramMessage(to: string, text: string) {
        await bot.telegram.sendMessage(to, text)
    }

    static async broadcastTelegramMessage(text: string) {
        const users = await Users.find()
        for (const user of users) {
            if (user.chat_id) {
                await bot.telegram.sendMessage(user.chat_id, text)
            }
        }
    }

    static async clearCountUserRequests() {
        await UsersRequests.update({}, { count: 0 })
    }

}