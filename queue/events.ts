import { CommandHandler } from "./commandHandler";
import queue from "../src/queue"

const channel = queue.createChannel('flights-bot', { durable: true }, { noAck: false })
channel.prefetch(1)

channel.event('send-message', async (json, ack) => {
    try {
        await CommandHandler.sendTelegramMessage(json.data.to, json.data.text)
        ack()
    } catch (e) {
        console.error(e)
    }
})


channel.event('broadcast', async (json, ack) => {
    try {
        await CommandHandler.broadcastTelegramMessage(json.data.text)
        ack()
    } catch (e) {
        console.error(e)
    }
})

channel.event('clear-user-requests', async (_json, ack) => {
    try {
        await CommandHandler.clearCountUserRequests()
        ack()
    } catch (e) {
        console.error(e)
    }
})

export default channel
