import { Context } from "telegraf";
import { Update } from "telegraf/typings/core/types/typegram";
import { UsersRequests } from "../../entities/pg/usersRequests.entity";
import { Requests } from "../../entities/pg/requests.entity";

const ADMIN_TELEGRAM_ID = Number(process.env['ADMIN_TELEGRAM_ID'])

export function registrRequest(name: string) {
    return async (ctx: Context<Update>, next: () => Promise<void>) => {
        if (ctx.updateType == 'message') {
            try {
                let req = await Requests.findOne({ where: { name } })
                if (!req) {
                    const newReq = new Requests()
                    newReq.name = name
                    await newReq.save()
                    req = newReq
                }

                let userRequest = await UsersRequests.findOne({ where: { request_id: req.id, user_id: ctx.state.user.id } })
                if (!userRequest) {
                    const newUserReq = new UsersRequests()
                    newUserReq.user_id = ctx.state.user.id
                    newUserReq.request_id = req.id
                    newUserReq.limit = req.defaultLimit
                    newUserReq.count = 0
                    await newUserReq.save()
                    userRequest = newUserReq
                }

                if (userRequest.count >= userRequest.limit) {
                    return await ctx.reply(`Вы превысили лимит запросов, обратитесь к [Администратору](tg://user?id=${ADMIN_TELEGRAM_ID})`, { parse_mode: "MarkdownV2" })
                }

                userRequest.count += 1
                userRequest.lastUpdate = new Date()
                await next()
                await userRequest.save()
            } catch (e) {
                console.log(e)
                await ctx.reply('Произошла ошибка. Попробуйте снова.')
            }
        }
    }
}