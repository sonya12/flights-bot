import { Context } from "telegraf";
import { Update } from "telegraf/typings/core/types/typegram";
import { Users } from "../../entities/pg/users.entity";


export async function authUser(ctx: Context<Update>, next: () => Promise<void>) {
    if (ctx.updateType == 'message') {
        try {
            const candidat = await Users.findOne({ where: { chat_id: ctx.chat?.id } })
            if (!candidat) {
                const user = new Users()
                user.username = (<any>ctx.chat)?.username ?? ''
                user.first_name = (<any>ctx.chat)?.first_name ?? ''
                user.chat_id = ctx.chat?.id ?? -1
                ctx.state.user = user
                await user.save()
            } else {
                candidat.username = (<any>ctx.chat)?.username ?? ''
                candidat.first_name = (<any>ctx.chat)?.first_name ?? ''
                candidat.last_name = ctx.from?.last_name ?? ''
                ctx.state.user = candidat
                await candidat.save()
            }
            return next()
        } catch (e) {
            console.log(e)
            await ctx.reply('Произошла ошибка. Попробуйте снова.')
        }
    }
}