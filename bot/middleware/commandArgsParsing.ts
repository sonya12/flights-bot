import { Context } from "telegraf";
import { Update } from "telegraf/typings/core/types/typegram";


export function commandArgsParsing(ctx: Context<Update>, next: () => Promise<void>) {

    if (ctx.updateType == 'message' && ctx.message && 'text' in ctx.message) {
        const text = ctx.message.text;
        if (text.startsWith('/')) {
            const match = text.match(/^\/([^\s]+)\s?(.+)?/) || [];
            const args = match[2] ? match[2].split(' ') : [];
            const command = match[1];
            ctx.state.command = {
                raw: text,
                command,
                args: args.filter(a => a.length > 0),
            };
        }
    }
    return next()
}
