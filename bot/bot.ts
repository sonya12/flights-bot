import { Telegraf } from 'telegraf'
import { commandArgsParsing } from './middleware/commandArgsParsing'
import { authUser } from './middleware/authUser'


const TGBOT_TOKEN: string = process.env['TGBOT_TOKEN'] ?? ''

if (!TGBOT_TOKEN) {
    console.error('[ERROR] TGBOT_TOKEN is empty!')
    process.exit(1)
}

export const bot = new Telegraf(TGBOT_TOKEN)


bot.start(async ctx => {
    await ctx.reply('Привет\\!\nБот умеет присылать нотамы файлами\\. Для получения резервирований по США пришли любой текст\\.\n' +
        'Если хочешь получить исходник резервирования для любой точки ИКАО, пришли команду /source и точку ИКАО, например `/source KZAK` ' +
        'Если хочешь получить карту с нанесенными резервированиями пришли команду `/map #USA`',
        { parse_mode: 'MarkdownV2', reply_markup: { remove_keyboard: true } })
})

bot.use(authUser)
bot.use(commandArgsParsing)

