import { LessThan, MoreThan, Timestamp } from "typeorm"
import { FlightsEntity } from "../entities/pg/flights.entity"
import cache from "../src/cache"
import { filterFlights, FilterOptions } from "../src/flightFilter"
import { NowFlight } from "../src/flights"
import { MergeFlights } from "../src/mergeFlights"
import { AbstractSource } from "./updaters/abstsractSource"
import { ADSBSource } from "./updaters/adsbsource "


const filters: FilterOptions[] = [
    { type: 'E6' }, { type: "B742" }, { type: "B703" }, { type: 'E3CF' }, { type: 'E3TF' }, { type: "B52" }, { type: "R135" }, { type: "K35R" }, { type: "C17" },
    { type: "C5M" }, { type: "C130" }, { type: "P8" }, { type: "BE20", callsing: "YANK" }, { type: "Q4" }, { type: "B752", callsing: "SAM" },
    { callsing: "CMB" }
]

class FetchFlightsService {

    private updaters: AbstractSource[] = []

    private fnUpdate: NodeJS.Timer
    private fnClear: NodeJS.Timer

    public secondsToInactive = 120 as const // 2m
    public secondsToOld = 21_600 as const // 6h
    public landingAltitude = 100 as const

    constructor() {
        this.updaters.push(new ADSBSource())
    }

    async initialize() {
        //remove old flights
        await FlightsEntity.delete({ last_update: LessThan(Date.now() - (1000 * this.secondsToOld)) })
    }

    startUpdate() {
        this.fnUpdate = setInterval(async () => {
            try {
                const result: NowFlight[] = []
                for (const updater of this.updaters) {
                    result.push(...await updater.fetch())
                }
                await this.handleFlights(filterFlights(MergeFlights.merge(result), filters))                
            } catch (error: any) {
                console.log("error fetch", error.message)
            }
        }, 30000)

        this.fnClear = setInterval(async () => {
            await this.clearFlights()
        }, 30000)
    }

    stopUpdate() {
        clearInterval(this.fnUpdate)
        clearInterval(this.fnClear)
    }

    private async handleFlights(nowFlights: NowFlight[]) {
        for (const nfl of nowFlights) {
            const flight = await FlightsEntity.upsert(
                {
                    last_update: Date.now(),
                    hexcode: nfl.hexcode,
                    callsign: nfl.callsign,
                    reg: nfl.reg,
                    type: nfl.type,
                    country: nfl.country,
                    lat: nfl.lat,
                    lon: nfl.lon,
                    alt: nfl.alt,
                    course: nfl.course,
                    speed: nfl.speed,
                    image: nfl.image[0],
                    image_scale: nfl.image[1]
                },
                { conflictPaths: { hexcode: true } })

            const flight_id: string = flight.generatedMaps[0].flight_id
            await cache.pushToList(flight_id, JSON.stringify({
                lat: nfl.lat,
                lon: nfl.lon,
                alt: nfl.alt,
                course: nfl.course,
                speed: nfl.speed,
                timestamp: nfl.timestamp
            }), this.secondsToOld)
        }
    }

    private async clearFlights() {
        //remove landing flights
        const landingFlights = await FlightsEntity.find({
            where: [
                { alt: LessThan(this.landingAltitude), last_update: LessThan(Date.now() - (1000 * this.secondsToInactive)) },
                { alt: LessThan(this.landingAltitude * 1.5), last_update: LessThan(Date.now() - (1000 * this.secondsToInactive * 1.5)) },
                { alt: LessThan(this.landingAltitude * 2), last_update: LessThan(Date.now() - (1000 * this.secondsToInactive * 2)) },
                { alt: LessThan(this.landingAltitude * 4), last_update: LessThan(Date.now() - (1000 * this.secondsToInactive * 4)) },
                { alt: LessThan(this.landingAltitude * 8), last_update: LessThan(Date.now() - (1000 * this.secondsToInactive * 8)) },
            ]

        })
        if (landingFlights.length > 0)
            console.log(new Date(), "landingFlights:", landingFlights.length, landingFlights.map(fl => fl.callsign).join(','))

        for (const flight of landingFlights) {
            await cache.del(flight.flight_id)
            await flight.remove()
        }

        //remove old flights
        const oldFlights = await FlightsEntity.find({ where: { last_update: LessThan(Date.now() - (1000 * this.secondsToOld)) } })
        if (oldFlights.length > 0)
            console.log(new Date(), "oldFlights:", oldFlights.length, oldFlights.map(fl => fl.callsign).join(','))

        for (const flight of oldFlights) {
            await cache.del(flight.flight_id)
            await flight.remove()
        }
    }
}

export const fetchFlightsService = new FetchFlightsService()