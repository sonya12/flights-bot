import { ILike, In, MoreThan } from "typeorm";
import { FlightsEntity } from "../entities/pg/flights.entity";
import { FlightsRepository } from "../entities/clickhouse/FlightsRepository";
import { fetchFlightsService } from "./fetchFlightsService";


export class HistoricalFlightService {


    static async save() {
        const flights = await FlightsEntity.find({
            where: [
                { type: 'E6', last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) },
                { type: 'B742', reg: In(['73-1676', '74-0787']), last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) },
                { type: 'E3CF', last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) },
                { type: 'E3TF', last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) },
                { type: 'R135', last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) },
                { type: 'C17', callsign: ILike('RCH%'), last_update: MoreThan(Date.now() - (1000 * fetchFlightsService.secondsToInactive)), alt: MoreThan(100) }
            ]
        })
        if (flights.length == 0)
            return

        await FlightsRepository.saveFlights(flights)
        console.log(new Date(), "saveFlights:", flights.length)
    }

}