import { NowFlight } from "../../src/flights";

export abstract class AbstractSource {
    public abstract fetch(): Promise<NowFlight[]>
}