import axios from "axios"
import fs from 'fs'
import { flagByHexcode } from "../../src/flightCountry";
import { flightImages } from "../../src/flightImages";
import { NowFlight } from "../../src/flights";
import { AbstractSource } from "./abstsractSource";

//TODO: refactor
export class ADSBSource extends AbstractSource {

    private wasi: any
    private memory: Uint8Array

    constructor() {
        super()
        const wasm_imports = {
            env: {
                emscripten_notify_memory_growth: () => {
                    this.memory = new Uint8Array(this.wasi.memory.buffer);
                    // console.log('zstddec: wasm memory size increased to: ' + this.memory.byteLength);
                },
            },
        };

        const buf = fs.readFileSync(__dirname + '/../../files/zstd.wasm');
        WebAssembly.instantiate(new Uint8Array(buf), wasm_imports)
            .then((res: any) => res.instance.exports)
            .then(exports => {
                this.wasi = exports
                wasm_imports.env.emscripten_notify_memory_growth();
            });
    }

    async fetch() {
        try {
            const http_response = await axios.get('https://globe.adsbexchange.com/re-api/?binCraft&zstd&box=-89.991674,89.999158,-180,180', {
                headers: {
                    'x-requested-with': "XMLHttpRequest",
                    "Referer": "https://globe.adsbexchange.com/",
                    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 YaBrowser/23.1.2.928 Yowser/2.5 Safari/537.36"
                },
                responseType: "arraybuffer"
            })

            const Q = http_response.data.byteLength
            const C = this.wasi.malloc(Q);
            const A = http_response.data
            this.memory.set(A, C);

            const I = this.wasi.ZSTD_findDecompressedSize(C, Q);
            const E = this.wasi.malloc(I);

            const decode_data = this.wasi.ZSTD_decompress(E, I, C, Q)
            let D = this.memory.slice(E, E + decode_data);
            this.wasi.free(C);
            this.wasi.free(E);

            return this.decodeFlights(D)
        } catch (error: any) {
            console.log(error.message)
            return []
        }
    }

    private decodeFlights(data: any) {
        const buffer = data.buffer;
        const INT32_MAX = 2147483647;
        let u32 = new Uint32Array(data.buffer, 0, 12);
        let stride = u32[2];

        const aircrafts: NowFlight[] = []
        for (let off = stride; off < buffer.byteLength; off += stride) {

            let s32 = new Int32Array(buffer, off, stride / 4);
            let u16 = new Uint16Array(buffer, off, stride / 2);
            let s16 = new Int16Array(buffer, off, stride / 2);
            let u8 = new Uint8Array(buffer, off, stride);
            let t = s32[0] & (1 << 24);

            const hexcode = (s32[0] & ((1 << 24) - 1)).toString(16).padStart(6, '0');

            let callsign = '';
            for (let i = 78; u8[i] && i < 86; i++) {
                callsign += String.fromCharCode(u8[i]).trim();
            }

            let type = '';
            for (let i = 88; u8[i] && i < 92; i++) {
                type += String.fromCharCode(u8[i]);
            }

            let reg = '';
            for (let i = 92; u8[i] && i < 104; i++) {
                reg += String.fromCharCode(u8[i]);
            }

            const category = u8[64].toString(16).toUpperCase();
            const image = flightImages(category, type)

            aircrafts.push({
                reg, callsign, type,
                hexcode: t ? '~' + hexcode : hexcode,
                country: flagByHexcode(hexcode),
                lat: s32[3] / 1e6,
                lon: s32[2] / 1e6,
                timestamp: Date.now(),
                alt: Math.round((s16[11] * 25 || u16[12] * 4 || u16[13] * 4 || s16[10] * 25) * 0.3048),
                course: Math.round(s16[20] / 90),
                speed: Math.round((s16[17] / 10) / 0.514444445),
                image: image
            });
        }

        return aircrafts;
    }

}