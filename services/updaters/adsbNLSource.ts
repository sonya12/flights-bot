import axios from "axios"
import * as HTMLParser from 'node-html-parser';
import { NowFlight } from "../../src/flights";
import { AbstractSource } from "./abstsractSource";


export class ADSBNLSource extends AbstractSource {

    async fetch() {
        try {
            const flights: NowFlight[] = []

            const responseElseWhere = await axios.get('https://www.ads-b.nl/index.php?pageno=2002')
            const responseEurope = await axios.get('https://www.ads-b.nl/index.php?pageno=2001')

            flights.push(...this.parseFlights(responseElseWhere.data.toString()))
            flights.push(...this.parseFlights(responseEurope.data.toString()))

            return flights
        } catch (error: any) {
            console.log(error.message)
            return []
        }
    }

    private parseFlights(data: string) {
        const flights: NowFlight[] = []

        const root = HTMLParser.parse(data, {
            lowerCaseTagName: true,  // convert tag name to lower case (hurt performance heavily)
            comment: false,            // retrieve comments (hurt performance slightly)
            blockTextElements: {
                script: true,	// keep text content when parsing
                noscript: true,	// keep text content when parsing
                style: true,		// keep text content when parsing
                pre: true			// keep text content when parsing
            }
        })

        const elements = root.querySelectorAll("[style*='background-color: powderblue; border-bottom: 1px solid white']")

        for (const d of elements) {
            const data1 = d.querySelectorAll('div.col-2.kolom')
            const [reg, type, callsign] = data1.map(el => el.rawText.replace('&nbsp;', ''))
            const data2 = d.querySelectorAll('div.col-1.kolom')
            const [, speed, alt, course] = data2.map(el => el.rawText.replace('&nbsp;', ''))

            const data3 = d.querySelectorAll('div.col-1.kolom img[title]')
            const country = data3[1].attributes['title']
            const [lat, lon] = data3[2].attributes['title'].match(/-?\d+.\d+/gm) ?? []

            flights.push({
                reg, type, callsign, country,
                timestamp: Date.now(),
                speed: Number(speed),
                alt: Math.round(Number(alt) * 0.3048),
                course: Number(course),
                lat: Number(lat),
                lon: Number(lon),
                hexcode: "",
                image: ['unknown', 1]
            })
        }
        return flights
    }
}